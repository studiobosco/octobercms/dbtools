<?php namespace StudioBosco\DBTools;

use Backend;
use System\Classes\PluginBase;

/**
 * DBTools Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'DBTools',
            'description' => 'Tools for database management and migration',
            'author'      => 'Studio Bosco',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand(
            'dbtools.importcsv',
            'StudioBosco\DBTools\Console\ImportCsv'
        );
        $this->registerConsoleCommand(
            'dbtools.importjson',
            'StudioBosco\DBTools\Console\ImportJson'
        );
        $this->registerConsoleCommand(
            'dbtools.exportjson',
            'StudioBosco\DBTools\Console\ExportJson'
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate
    }
}
