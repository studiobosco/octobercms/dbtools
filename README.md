# DB Tools

Provides tools to manage and import / export databases in OctoberCMS.

## Usage

After installing you will get new artisan command under the "dbtools" namespace:

- dbtools:exportjson   Exports JSON files from database
- dbtools:importjson   Imports JSON file to database
- dbtools:importcsv    Imports CSV files to database
