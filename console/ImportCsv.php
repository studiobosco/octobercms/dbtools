<?php namespace StudioBosco\DBTools\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Db;
use Schema;
use Config;

class ImportCsv extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'dbtools:importcsv';

    /**
     * @var string The console command description.
     */
    protected $description = 'Imports CSV files into the database';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $inputDir = $this->argument('directory');
        $seperator = $this->option('seperator');
        $stringQualifier = $this->option('string-qualifier');
        $connection = $this->option('connection');

        if (!is_dir($inputDir)) {
            $this->error($inputDir . ' is not a directory.');
            return;
        }

        $files = scandir($inputDir);

        foreach($files as $file) {
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            if ($ext === 'csv') {
                $this->importCsv(
                    $inputDir . '/' . $file,
                    $seperator,
                    $stringQualifier,
                    $connection
                );
            }
        }
        // $this->output->writeln('Hello world!');
    }

    protected function importCsv(
        string $file,
        string $seperator = ',',
        string $stringQualifier = '"',
        string $connection = null
    )
    {
        if (!$connection) {
            $connection = Config::get('database.default');
        }

        $dbConnection = Db::connection($connection);
        $dbSchema = Schema::connection($connection);
        $table = basename($file, '.csv');

        $this->info('Importing ' . $file . ' into table "' . $table . '" ...');

        $fileHandle = fopen($file, 'r');

        if (!$fileHandle) {
            $this->warning('Cannot open file ' . $file . '. Skipping.');
            return;
        }

        // fetch first line for col names
        $cols = fgetcsv($fileHandle, 0, $seperator, $stringQualifier);
        $data = [];

        while (($row = fgetcsv($fileHandle, 0, $seperator, $stringQualifier)) !== false) {
            $data[] = array_combine($cols, $row);
        }

        fclose($fileHandle);

        // ignore non existing tables
        if (!$dbSchema->hasTable($table)) {
            $this->info('Table "' . $table . '" does not exists in database. Skipping.');
            return;
        }

        // import everything in a single transaction
        $dbConnection->beginTransaction();

        try {
            // empty the table
            $dbConnection->table($table)->truncate();

            foreach($data as $item) {
                $itemId = array_get($item, 'id', array_get($item, 'ID', null));

                // transform empty strings to null values
                foreach($item as $i => $value) {
                    if ($value === '') {
                        $item[$i] = null;
                    }
                }

                if (
                    $itemId &&
                    $dbConnection->table($table)->select('id')->where('id', $itemId)->count() > 0
                ) {
                    // overwrite existing entries
                    $dbConnection->table($table)->where('id', $itemId)->update($item);
                } else {
                    // insert new entry
                    $dbConnection->table($table)->insert($item);
                }
            }
            $dbConnection->commit();
        } catch (\Exception $err) {
            // rollback in case of an error
            $dbConnection->rollBack();
            $this->error($err);
            return;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['directory', InputArgument::REQUIRED, 'Directory with one CSV file per table.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['seperator', 's', InputOption::VALUE_OPTIONAL, 'Value seperator', ','],
            ['string-qualifier', 'sq', InputOption::VALUE_OPTIONAL, 'String qualifier', '"'],
            ['connection', 'c', InputOption::VALUE_OPTIONAL, 'Database connection to import data into.', null],
        ];
    }
}
