<?php namespace StudioBosco\DBTools\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Db;
use Schema;
use Config;

class ImportJson extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'dbtools:importjson';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import JSON files into the database';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $inputDir = $this->argument('directory');
        $connection = $this->option('connection');

        if (!is_dir($inputDir)) {
            $this->error($inputDir . ' is not a directory.');
            return;
        }

        $files = scandir($inputDir);

        foreach($files as $file) {
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            if ($ext === 'json') {
                $this->importJson(
                    $inputDir . '/' . $file,
                    $connection
                );
            }
        }
    }

    protected function importJson(
        string $file,
        string $connection = null
    )
    {
        if (!$connection) {
            $connection = Config::get('database.default');
        }

        $dbConnection = Db::connection($connection);
        $dbSchema = Schema::connection($connection);
        $table = basename($file, '.json');

        $this->info('Importing ' . $file . ' into table "' . $table . '" ...');

        try {
            $json = file_get_contents($file);
        } catch (\Exception $err) {
            $this->warn('Unable to load file ' . $file . '. Skipping.');
            return;
        }

        try {
            $rows = json_decode($json, true);
        } catch (\Exception $err) {
            $this->warn('Unable to parse json in file ' . $file . '. Skipping.');
            return;
        }

        // ignore non existing tables
        if (!$dbSchema->hasTable($table)) {
            $this->info('Table "' . $table . '" does not exists in database. Skipping.');
            return;
        }

        // import everything in a single transaction
        $dbConnection->beginTransaction();

        try {
            // empty the table
            $dbConnection->table($table)->truncate();

            foreach($rows as $item) {
                $itemId = array_get($item, 'id', array_get($item, 'ID', null));

                // transform empty strings to null values
                foreach($item as $key => $value) {
                    if ($value === '') {
                        $item[$key] = null;
                    }
                }

                if (
                    $itemId &&
                    $dbConnection->table($table)->select('id')->where('id', $itemId)->count() > 0
                ) {
                    // overwrite existing entries
                    $dbConnection->table($table)->where('id', $itemId)->update($item);
                } else {
                    // insert new entry
                    $dbConnection->table($table)->insert($item);
                }
            }
            $dbConnection->commit();
        } catch (\Exception $err) {
            // rollback in case of an error
            $dbConnection->rollBack();
            $this->error($err);
            return;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['directory', InputArgument::REQUIRED, 'Directory with one JSON file per table.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['connection', 'c', InputOption::VALUE_OPTIONAL, 'Database connection to import data into.', null],
        ];
    }
}
