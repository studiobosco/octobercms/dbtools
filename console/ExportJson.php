<?php namespace StudioBosco\DBTools\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Db;
use Schema;
use Config;

class ExportJson extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'dbtools:exportjson';

    /**
     * @var string The console command description.
     */
    protected $description = 'Export JSON files from the database';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $outputDir = $this->argument('directory');
        $connection = $this->option('connection');

        if (!is_dir($outputDir)) {
            $this->error($outputDir . ' is not a directory.');
            return;
        }

        $this->exportJson($outputDir, $connection);
    }

    protected function exportJson(
        string $dir,
        string $connection = null
    )
    {
        if (!$connection) {
            $connection = Config::get('database.default');
        }
        $dbDriver = Config::get('database.connections.' . $connection . '.driver');
        $dbConnection = Db::connection($connection);
        $dbSchema = Schema::connection($connection);

        switch($dbDriver) {
            case 'sqlite':
                $tables = array_map('reset', $dbConnection->select("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"));
                break;
            case 'mysql':
                $tables = array_map('reset', $dbConnection->select('SHOW TABLES'));
                break;
            case 'postgres':
                // TODO: get tables in postgres
                $tables = [];
                break;
            default:
                $tables = [];
        }

        foreach($tables as $table) {
            $file = $table . '.json';
            $this->info('Exporting table "' . $table . '" to ' . $file . ' ...');

            $rows = $dbConnection->table($table)->select('*')->get();

            $json = json_encode($rows, JSON_PRETTY_PRINT);

            file_put_contents($dir . '/' . $file, $json);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['directory', InputArgument::REQUIRED, 'Directory with one CSV file per table.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['seperator', 's', InputOption::VALUE_OPTIONAL, 'Value seperator', ','],
            ['string-qualifier', 'sq', InputOption::VALUE_OPTIONAL, 'String qualifier', '"'],
            ['connection', 'c', InputOption::VALUE_OPTIONAL, 'Database connection to import data into.', null],
        ];
    }
}
